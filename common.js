let saveTheme = localStorage.getItem('theme');

const content = document.getElementById('content');

if (saveTheme) {
    content.classList.add(saveTheme);
} else {
    content.classList.add('light');
    const changeBackground = document.querySelector('.main_box');
    changeBackground.style.backgroundColor = 'green';
}

const userThemes = document.querySelectorAll('.user_theme');
userThemes.forEach(theme => {
    theme.addEventListener('click', () => {
        changeTheme();
    });
});

function changeTheme() {
    let currentTheme = document.body.classList.contains('light') ? 'light' : 'dark';
    let newTheme = currentTheme === 'light' ? 'dark' : 'light';
    
    document.body.classList.remove(currentTheme);
    document.body.classList.add(newTheme);
    
    localStorage.setItem('theme', newTheme);
}